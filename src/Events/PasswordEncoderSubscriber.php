<?php

namespace App\Events;

use ApiPlatform\Symfony\EventListener\EventPriorities;
use App\Entity\User;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class PasswordEncoderSubscriber implements EventSubscriberInterface
{
    /**
     * @var UserPasswordHasherInterface
     */
    private UserPasswordHasherInterface $encoder;

    public function __construct(UserPasswordHasherInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::VIEW => ['EncodePassword', EventPriorities::PRE_WRITE]
        ];
    }

    /**
     * @param ViewEvent $viewEvent
     * @return void
     */
    public function EncodePassword(ViewEvent $viewEvent): void
    {
        $data = $viewEvent->getControllerResult();
        $method = $viewEvent->getRequest()->getMethod();
        if ($data instanceof User && $method == 'POST') {
            $hash = $this->encoder->hashPassword($data, $data->getPassword());
            $data->setPassword($hash);
        }
    }
}